#Base Image
FROM snakemake/snakemake:latest

# Programs needed
RUN apt-get update

RUN conda config --add channels defaults &&\
    conda config --add channels bioconda &&\
    conda config --add channels conda-forge &&\
    conda config --set channel_priority strict

RUN conda update conda
RUN conda install mamba
RUN mamba install mafft=7.508 -y
RUN mamba install raxml-ng=1.1.0 -y


# Directory
WORKDIR /AliPhy

#Scripts
COPY Merge.sh /AliPhy
COPY AliPhy.sh /AliPhy

CMD ["/bin/bash"]


