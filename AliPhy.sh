#!/bin/bash
#align (mafft) 
/usr/bin/mafft --auto --reorder --anysymbol "merged.fasta" > "Align.fasta"
echo "ALignment done"
#To use the raxml-ng we have to copy the aligned fasta into the directory and run on the same directory
#copy aligned fasta into raxml-ng_v1.1.0_linux_x86_64 directory
cp ./Align.fasta ./raxml-ng_v1.1.0_linux_x86_64
echo 'copied to./raxml-ng_v1.1.0_linux_x86_64' 
#go into the raxml-ng_v1.1.0_linux_x86_64 directory
cd raxml-ng_v1.1.0_linux_x86_64/
echo 'entered ./raxml-ng_v1.1.0_linux_x86_64 directory'
#run phylogeny with raxml-ng
# see if there is any dulpicates Taxon,invalid characters in taxon names, duplicated sequences, fully undetermined('gaponly') sequences and columns and incorrect or incompatible evolutionary models, partitioning scheme and starting tress(if provided)
./raxml-ng --check --msa Align.fasta --model PROTGTR
echo 'checking sanity'
#it will give a fixed fasta file Ali.fasta.raxml.reduced.phy, and then we check again if it has no anomalies
./raxml-ng --check --msa Align.fasta.raxml.reduced.phy --model PROTGTR
#do the phylogeny tree
echo 'starting phylogeny tree with 100 boostrap'
./raxml-ng --all --msa Align.fasta.raxml.reduced.phy --model PROTGTR --prefix T1 --threads 2 --seed 2 --tree pars{10},rand{10} --bs-trees 100 
#create a dictory for the files created
mkdir Tree_files
echo 'directory for resuling files created'
#move said files into the directory
mv ./Align.fasta ./Tree_files
mv ./Align.fasta.raxml.log ./Tree_files
mv ./Align.fasta.raxml.reduced.phy ./Tree_files
mv ./Align.fasta.raxml.reduced.phy.raxml.log ./Tree_files
mv ./T1.raxml.bestModel ./Tree_files
mv ./T1.raxml.bestTree ./Tree_files
mv ./T1.raxml.bootstraps ./Tree_files
mv ./T1.raxml.log ./Tree_files
mv ./T1.raxml.mlTrees ./Tree_files
mv ./T1.raxml.rba ./Tree_files
mv ./T1.raxml.bestTreeCollapsed ./Tree_files
mv ./T1.raxml.startTree ./Tree_files
mv ./T1.raxml.support ./Tree_files
echo 'Files moved into directory'
echo 'To visualize and edit the tree, you can open the support file in figtree'
echo 'done :)'
echo 'script created by Daniela Deodato year 2023'
