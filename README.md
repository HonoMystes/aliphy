# Intructions

## Requirements

You must have docker installed.

If you don't have the docker installed you can see [here](https://www.docker.com/) how to.

## First things first
Copy this comands to obtain the program.

`git clone --recurse-submodules https://gitlab.com/HonoMystes/aliphy.git`

`docker pull honomystes/aliphy`

## Running
To run the program you have to go into the merge file and write the names of the fastas you want to align.

Next you are going to run the command of that script. 

The script also works if you just put one file it will however change the name to make it able to run the next script.

`sh Merge.sh`

After you have the merged fasta you run the alignment and phylogeny program.

`sh AliPhy.sh`


You can then vizualize the tree by opening the support file in any program that reads newick files, for example [figtree](http://tree.bio.ed.ac.uk/software/figtree/).


## Attention
For the files to merge you have to put them in the same folder (directory) where the scripts are.

Beware of the spaces when editing the Merge.sh script, follow the example in the script.
