#!/bin/bash

#merge of fasta you are going to use
#Please substitute the fasta with your fasta like in the example
#Example:
#cat M9_marineVibrio.fasta collagenase_G_from_Clostridium_histolyticum.fasta Crystal_structure_of_Grimontia_hollisae.fasta Structure_of_Vibrio_collagenase.fasta > merged.fasta
cat File.fasta > merged.fasta
echo 'Merge Fasta Complete please procede to execute the comand sh ALiPhy.sh'
echo 'script created by Daniela Deodato year 2023'
